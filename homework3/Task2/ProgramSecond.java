public class ProgramSecond {
	public static void main(String[] args) {
        int[] ns = new int[] {10, 100, 1_000, 10_000, 100_000, 1_000_000, 10_000_000};
        printIntegralResultsForN(1.2, 2, ns);

    }

    public static double square(double x){
        return x * x;
    }

    public static double f(double x){
        return Math.sqrt(1 + (2 * square(x)) - (square(x) * x));
    }

    public static double integralBySimpson(double a, double b, int n) {
        double h = (b - a) / n;
        double result = 0;
        double x = a + h;

        for (double k = 1; k < n; k += 2) {
            result = result + f(x - h) + (4 * f(x)) + f(x + h);
            x += h * 2;
        }

        return (result * h) / 3;
    }

	public static void printIntegralResultsForN(double a, double b, int[] ns){
		for (int i = 0; i < ns.length; i++){
			System.out.println("For N = " + ns[i] + 
				" , result = " + integralBySimpson(a, b, ns[i]));
		}
	}

}