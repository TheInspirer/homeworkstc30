public class ProgramSecond {
	public static void main(String[] args) {
		System.out.println(fibonRec(10)[0]);
	}

	public static int[] fibonRec(int searchNumber){
		if(searchNumber <= 1){
			int[] answer = {searchNumber, 0};
			return answer;
		} else {
			int[] temp = fibonRec(searchNumber - 1);
			int[] answer = {temp[0] + temp[1], temp[0]};
			return answer;
		}
	}
}