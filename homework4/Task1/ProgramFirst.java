class ProgramFirst {
	public static void main(String[] args) {
		int[] array = new int[] {1,2,3,4,5,6,7,8,10,21,38,39,42,45,49,70,81,91,100};
		System.out.println(binarySearch(array, 101, 0, array.length - 1));
	}

	public static boolean binarySearch(int[] array, int searchElement, int left, int right){
		if(right < left){
			return false;
		}

		int middle = (right + left) / 2;

		if(array[middle] < searchElement){
			left = middle + 1;
			return binarySearch(array, searchElement, left, right);
		} else if (array[middle] > searchElement){
			right = middle - 1;
			return binarySearch(array, searchElement, left, right);
		} else {
			return true;
		}
	}
}