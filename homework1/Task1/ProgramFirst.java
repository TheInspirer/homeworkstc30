public class ProgramFirst{
	public static void main(String[] args) {
		int number = 12345;
        int digitSum = 0;

        digitSum += number % 10;
        number /= 10;
        digitSum += number % 10;
        number /= 10;
        digitSum += number % 10;
        number /= 10;
        digitSum += number % 10;
        number /= 10;
        digitSum += number;

        System.out.println(digitSum);
	}
}