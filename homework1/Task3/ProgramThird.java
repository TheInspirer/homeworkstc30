public class ProgramThird{
    public static void main(String[] args) {
        int a = 119;
        boolean aSumIsSimple = false;
        int b = 33542;
        boolean bSumIsSimple = false;
        int c = 99;
        boolean cSumIsSimple = false;
        int d = 992;
        boolean dSumIsSimple = false;
        int e = 0;
        boolean eSumIsSimple = false;


        //Работаем с числом a, вычисляем сумму и простоту получившейся суммы
        int i = 0;
        int aSum = 0;
        int aCopy = a;

        while (aCopy != 0) {
            aSum += aCopy % 10;
            aCopy /= 10;
        }

        i = aSum;
        int flag = 0;

        while (i >= 2){
            if(flag < 2){
                aSumIsSimple = true;
                if(aSum % i == 0){
                    flag++;
                }
            }else {
                aSumIsSimple = false;
                //break;
            }
            i--;
        }
        System.out.println(aSum + " " + aSumIsSimple);

        //Работаем с числом b, вычисляем сумму и простоту получившейся суммы
        int bSum = 0;
        int bCopy = b;

        while (bCopy != 0) {
            bSum += bCopy % 10;
            bCopy /= 10;
        }

        i = bSum;
        flag = 0;

        while (i >= 2){
            if(flag < 2){
                bSumIsSimple = true;
                if(bSum % i == 0){
                    flag++;
                }
            }else{
                bSumIsSimple = false;
                //break;
            }
            i--;
        }
        System.out.println(bSum + " " + bSumIsSimple);

        //Работаем с числом с, вычисляем сумму и простоту получившейся суммы
        int cSum = 0;
        int cCopy = c;

        while (cCopy != 0) {
            cSum += cCopy % 10;
            cCopy /= 10;
        }

        i = cSum;
        flag = 0;

        while (i >= 2){
            if(flag < 2){
                cSumIsSimple = true;
                if(cSum % i == 0){
                    flag++;
                }
            }else{
                cSumIsSimple = false;
                //break;
            }
            i--;
        }
        System.out.println(cSum + " " + cSumIsSimple);

        //Работаем с числом с, вычисляем сумму и простоту получившейся суммы 
        int dSum = 0;
        int dCopy = d;

        while (dCopy != 0) {
            dSum += dCopy % 10;
            dCopy /= 10;
        }

        i = dSum;
        flag = 0;

        while (i >= 2){
            if(flag < 2){
                dSumIsSimple = true;
                if(dSum % i == 0){
                    flag++;
                }
            }else{
                dSumIsSimple = false;
                //break;
            }
            i--;
        }
        System.out.println(dSum + " " + dSumIsSimple);

        //Работаем с числом e, вычисляем сумму и простоту получившейся суммы
        int eSum = 0;
        int eCopy = e;

        while (eCopy != 0) {
            eSum += eCopy % 10;
            eCopy /= 10;
        }

        i = eSum;
        flag = 0;

        while (i >= 2){
            if(flag < 2){
                eSumIsSimple = true;
                if(eSum % i == 0){
                    flag++;
                }
            }else{
                eSumIsSimple = false;
                //break;
            }
            i--;
        }
        System.out.println(eSum + " " + eSumIsSimple);

        //Умножаем простые числа с простыми суммами
        int digitSimpleSum = 1;
        if(aSumIsSimple){
            digitSimpleSum *= a;
        }
        if(bSumIsSimple){
            digitSimpleSum *= b;
        }
        if (cSumIsSimple){
            digitSimpleSum *= c;
        }
        if (dSumIsSimple){
            digitSimpleSum *= d;
        }
        if (eSumIsSimple){
            digitSimpleSum *= e;
        }

        System.out.println(digitSimpleSum);
    }
}