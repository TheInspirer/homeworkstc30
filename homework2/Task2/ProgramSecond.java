import java.util.Scanner;
import java.util.Arrays;

//Второе задание
public class ProgramSecond {
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
        int[] array = new int[scn.nextInt()];


        for(int i=0; i < array.length; i++){
            array[i] = scn.nextInt();
        }

        int temp = 0;
        int flag = array.length - 1;
        for(int i=0; i < array.length/2; i++){
            temp = array[i];
            array[i] = array[flag];
            array[flag] = temp;
            flag--;
        }

        System.out.println(Arrays.toString(array));
	}
}