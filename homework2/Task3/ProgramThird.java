import java.util.Scanner;

// Третье задание
public class ProgramThird{
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in); 
		int array[] = new int[scn.nextInt()];
		int arrayAverage = 0;
		int sumArray = 0;

		for(int i=0; i < array.length; i++){
			array[i] = scn.nextInt();
			sumArray += array[i];
		}

		arrayAverage = sumArray / array.length;
		System.out.println(arrayAverage);
	}
}