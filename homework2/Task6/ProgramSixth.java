public class ProgramSixth{
	// Шестое задание
	public static void main(String[] args) {
		int array[] = {4, 2, 3, 5, 7};
		int number = 0;
		int flag = 1;

		for(int i = array.length - 1; i >= 0; i--){
			number += array[i] * flag;
			flag *= 10;
		}

		System.out.println(number);
	}
}