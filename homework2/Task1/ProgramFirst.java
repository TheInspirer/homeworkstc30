import java.util.Scanner;

//Первое задание
public class ProgramFirst{
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		System.out.println("Enter size of array: ");
		int[] array = new int[scn.nextInt()];
		System.out.println("Enter number in array: ");
		int arraySum = 0;

		for(int i = 0; i < array.length; i++){
			array[i] = scn.nextInt();
			arraySum += array[i];
		}

		System.out.println();
		System.out.println("Sum of array = " + arraySum);

	}
}