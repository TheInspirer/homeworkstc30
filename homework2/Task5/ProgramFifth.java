import java.util.Scanner;
import java.util.Random;

// Пятое задание
public class ProgramFifth {
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		Random rnd = new Random();
		System.out.println("Enter size of array: ");
		int[] array = new int[scn.nextInt()];
		int k = 0;

		// Enter array random numbers
		while(k < array.length){
			array[k] = rnd.nextInt(100) + 1;
			k++;
		}

		k = 0;
		// Display array
		while( k < array.length){
			System.out.print(array[k] + " ");
			k++; 
		}
		System.out.println();
		System.out.println("Use bubble sort: ");

		int temp = 0;
		// Bubble sort
		for(int i = 0; i < array.length; i++){
			for(int j = 1; j < array.length - i; j++){
				if(array[j - 1] > array[j]){
					temp = array[j-1];
					array[j-1] = array[j];
					array[j] = temp;
				}
			}
		}

		k = 0;
		// Display sort array
		while( k < array.length){
			System.out.print(array[k] + " ");
			k++; 
		}
		System.out.println();
	}
}