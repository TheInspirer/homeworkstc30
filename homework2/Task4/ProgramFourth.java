import java.util.Scanner;

// Четвертое задание
public class ProgramFourth {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter size of array: ");
		int[] array = new int[scanner.nextInt()];

		System.out.println("Enter numbers: ");
		for(int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
		}

		System.out.println();

		int max = array[0];
		int min = array[0];
		int indexMax = 0;
		int indexMin = 0;

		for(int i = 1; i < array.length; i++){
			if(max < array[i]){
				max = array[i];
				indexMax = i;
			}
			if(min > array[i]){
				min = array[i];
				indexMin = i;
			}
		}

		array[indexMin] = max;
		array[indexMax] = min;

		for (int i = 0; i < array.length; i++){
			System.out.print(array[i] + " ");
		}

		System.out.println();
	}
}