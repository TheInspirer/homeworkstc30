package ru.inno.singltone;

public class Main {
    public static void main(String[] args) {
        //Logger logger = new Logger("MyLogger");

        //Logger logger = Logger.newLogger("MyLogger");
        Logger logger = Logger.newLogger();
        logger.info("Программа запущена");
        logger.error("Какая-то проблема");
    }
}
