package ru.inno.singltone;
import java.time.LocalDateTime;

public class Logger {

    private final static Logger instance;

    static {
        instance = new Logger();
    }

    public static Logger newLogger(){
        return instance;
    }

    public void info(String message){
        System.out.println(createMessage(message));
    }

    public void error(String message){
        System.err.println(createMessage(message));
    }

    private String createMessage(String message){
        return LocalDateTime.now().toString() + " : " + message;
    }
}
