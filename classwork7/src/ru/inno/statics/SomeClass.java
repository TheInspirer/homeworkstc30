package ru.inno.statics;

import java.util.Random;

public class SomeClass {

    public static final int MAX_AGE = 100;

    public int b;

    // Статическое поле - глобальная переменная
    static int a;

    // Статический инициализатор (статический конструктор)
    static{
        Random random = new Random();
        a = random.nextInt(100);
    }

    public void someMethod(){
        this.b = 777;
        // Статическую переменную не писать с this
        a = 888;
    }

    public void someMethod1(int a){
        SomeClass.a = a;
    }

    public static void someStaticMethod(){
        //this.b = 5; Нельзя, потому что метод статический и не предполагает наличие обьекта
        a = 378;
    }
}
