package ru.inno.statics;

public class Main2 {

    public static void main(String[] args) {

        SomeClass anObject1 = new SomeClass();
        anObject1.b = 5;
        SomeClass anObject2 = new SomeClass();
        anObject2.b = 10;
        SomeClass anObject3 = new SomeClass();
        anObject3.b = 15;

//        SomeClass.a = 20;
        anObject1.someMethod();
        anObject1.someMethod1(37);

        SomeClass.someStaticMethod();

        System.out.println(anObject1.b + " " + anObject2.b + " " + anObject3.b + " " + SomeClass.a);
    }
}
