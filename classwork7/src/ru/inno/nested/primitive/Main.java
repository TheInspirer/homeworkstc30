package ru.inno.nested.primitive;

public class Main {
    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass();
        OuterClass.NestedClass nestedClass = new OuterClass.NestedClass();
        OuterClass.InnerClass innerClass = outerClass.new InnerClass();
    }
}
