package ru.inno.nested.primitive;

public class OuterClass {
    private int outerClassField;
    private static int outerClassStaticField;

    // StaticNestedClass, InnerClass - вложенные классы (Nested)
    // StaticNestedClass - вложенный класс (или статический вложенный, nested, static nested)
    // InnerClass - внутренний класс

    public static class NestedClass {
        public int nestedClassField;

        public void method(){
            System.out.println(nestedClassField + " " + outerClassStaticField);
        }
    }

    public class InnerClass {
        public int innerClassField;

        public void method(){
            System.out.println(innerClassField + " " + outerClassField + " " + outerClassStaticField);
        }
    }
}
