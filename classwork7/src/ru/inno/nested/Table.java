package ru.inno.nested;

public class Table {
    private static final int MAX_TABLE_SIZE = 10;

    private TableEntry table[];
    private int count = 0;

    public Table(){
        this.table = new TableEntry[MAX_TABLE_SIZE];
    }

    public void put(String key, int value){
        if(count < MAX_TABLE_SIZE){
            TableEntry tableEntry = new TableEntry(key, value);
            table[count] = tableEntry;
            count++;
        }else {
            System.err.println("Таблица переполнена");
        }
    }

    public int get(String key){
        for (int i = 0; i < count; i++) {
            TableEntry current = table[i];

            if(current.getKey().equals(key)){
                return current.getValue();
            }
        }
        System.err.println("Ключ не найден");
        return -1;
    }


    // Внутренний клас (inner) - ассоциирован с объектом обрамляющего класса
    public class TablePrinter {

        private String tableHeaderFormat;
        private String tableRowFormat;

        public TablePrinter(int keyWeight, int valueWeight) {
            this.tableHeaderFormat = "%" + keyWeight + "s|%" + valueWeight + "s\n";
            this.tableRowFormat = "%" + keyWeight + "s|%" + valueWeight + "d\n";
        }

        public void printTable(String keyName, String valueName){

            System.out.printf(tableHeaderFormat, keyName, valueName);
            for (int i = 0; i < count; i++) {
                System.out.printf(tableRowFormat, table[i].getValue(),table[i].getValue());
            }
        }
    }

    // Вложенный класс (вложенный статически)
    private static class TableEntry {
        String key;
        int value;

        TableEntry(String key, int value) {
            this.key = key;
            this.value = value;
        }

        String getKey() {
            return key;
        }

        int getValue() {
            return value;
        }
    }
}
