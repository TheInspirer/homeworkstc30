package ru.inno.nested;

public class Main {
    public static void main(String[] args) {
        Table table = new Table();
        table.put("Марсель", 26);
        table.put("Дмитрий", 28);
        table.put("Ильдар", 23);

        Table table1 = new Table();
        table1.put("Карина", 21);
        table1.put("Марина", 29);
        table1.put("Дельта", 45);

//        System.out.println(table.get("Марсель"));
//        System.out.println(table.get("Дмитрий"));
//        System.out.println(table.get("Ильдар"));
//        System.out.println(table.get("Карина"));

//        TablePrinter tablePrinter = new TablePrinter(table, 10, 10);
//        tablePrinter.printTable("Имя", "Возраст");


        Table.TablePrinter printer = table.new TablePrinter(10,10);
        printer.printTable("Имя", "Значение");

    }
}
