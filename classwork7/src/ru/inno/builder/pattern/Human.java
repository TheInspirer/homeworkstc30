package ru.inno.builder.pattern;

public class Human {
    private int age;
    private String firstName;
    private String lastName;
    private boolean isFinishedCourse;

    public Human(int age, String firstName, String lastName, boolean isFinishedCourse) {
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isFinishedCourse = isFinishedCourse;
    }

    public int getAge() {
        return age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean isFinishedCourse() {
        return isFinishedCourse;
    }
}
