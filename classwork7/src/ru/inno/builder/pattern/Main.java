package ru.inno.builder.pattern;

public class Main {
    public static void main(String[] args) {

//        Human human = Human.builder()
//            .firstName("Марсель")
//            .lastName("Сидиков")
//            .age(26)
//            .isFinishedCourse(true)
//            .build();

        Example example = new Example();
        example.append("Привет")
               .append("Как дела?")
               .append("Что нового");

        System.out.println(example.getText());
    }
}
