package ru.inno.builder.primitive;

public class Box {
    private int a;
    private int b;

    private Box(int a, int b){
        this.a = a;
        this.b = b;
    }

    public static Box createBox(int a, int b){
        return new Box(a ,b);
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }
}
