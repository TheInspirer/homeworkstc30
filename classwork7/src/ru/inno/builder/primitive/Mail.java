package ru.inno.builder.primitive;

public class Mail {
    public static void main(String[] args) {
        Box box = Box.createBox(10, 15);

        System.out.println(box.getA() + " " + box.getB());
    }
}
