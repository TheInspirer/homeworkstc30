public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;



    public static class Builder {
        private User newUser;

        public Builder() {
            newUser = new User();
        }

        public Builder withFirstName(String firstName){
            newUser.firstName = firstName;
            return this;
        }

        public Builder withLastName(String lastName){
            newUser.lastName = lastName;
            return this;
        }

        public Builder withAge(int age){
            newUser.age = age;
            return this;
        }

        public Builder withJob(boolean isWorker){
            newUser.isWorker = isWorker;
            return this;
        }

        public User build(){
            return newUser;
        }
    }

    @Override
    public String toString() {
        return "firstName= " + firstName + ", lastName='" + lastName + ", age=" + age + ", isWorker=" + isWorker;
    }
}
