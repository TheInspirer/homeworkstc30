public class Main {

    public static void main(String[] args) {
        User user = new User.Builder()
                .withFirstName("Дмитрий")
                .withLastName("Масюков")
                .withAge(27)
                .withJob(true)
                .build();

        User user1 = new User.Builder()
                .withFirstName("Ильдар")
                .withAge(29)
                .build();

        System.out.println(user);
        System.out.println(user1);

    }
}
