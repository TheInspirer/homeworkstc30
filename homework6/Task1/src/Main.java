public class Main {
    public static void main(String[] args) {
        Tv tv = new Tv("0738ASX", "Samsung");
        RemoteController remoteController = new RemoteController("9738ASR");
        remoteController.goToTv(tv);

        Channel channel1 = new Channel("Discovery");
        Channel channel2 = new Channel("NationalGeographic");
        Channel channel3 = new Channel("2x2");

        channel1.goToTv(tv);
        channel2.goToTv(tv);
        channel3.goToTv(tv);

        Program p1 = new Program("Simpsons");
        Program p2 = new Program("Top Gear");
        Program p3 = new Program("Битва за контейнеры");
        Program p4 = new Program("Как это сделано");
        Program p5 = new Program("Служба спасения Аляски");
        Program p6 = new Program("Инстинкт выживания");
        Program p7 = new Program("Дикий тунец");
        Program p8 = new Program("Naruto");
        Program p9 = new Program("Bliatch");

        p1.goToChannel(channel3);
        p2.goToChannel(channel1);
        p3.goToChannel(channel1);
        p4.goToChannel(channel1);
        p5.goToChannel(channel2);
        p6.goToChannel(channel2);
        p7.goToChannel(channel2);
        p8.goToChannel(channel3);
        p9.goToChannel(channel3);


        tv.show(1);

    }
}
