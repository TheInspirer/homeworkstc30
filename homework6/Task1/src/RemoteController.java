public class RemoteController {
    private Tv tv;
    private String model;

    public RemoteController(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public void goToTv(Tv tv){
        this.tv = tv;
        this.tv.takeRemoteController(this);
    }

    public void getRandomProgram(){
        tv.show(1);
    }
}
