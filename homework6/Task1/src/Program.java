public class Program {
    private Channel channel;
    private String nameProgram;

    public Program(String nameProgram) {
        this.nameProgram = nameProgram;
    }

    public void goToChannel(Channel channel){
        this.channel = channel;
        this.channel.takeProgram(this);
    }

    public String getNameProgram() {
        return nameProgram;
    }

    @Override
    public String toString() {
        return "Текущая программа: " + nameProgram;
    }
}
