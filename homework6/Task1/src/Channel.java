import java.util.Random;

public class Channel {
    private String nameChannel;
    private Random rnd;

    private Tv tv;
    private Program programs[];
    private int countOfProgram;

    public Channel(String nameChannel) {
        this.nameChannel = nameChannel;
        this.programs = new Program[3];
    }

    public String getNameChannel() {
        return nameChannel;
    }

    public void takeProgram(Program program){
        this.programs[countOfProgram] = program;
        this.countOfProgram++;
    }

    public void goToTv(Tv tv){
        this.tv = tv;
        this.tv.takeChannel(this);
    }

    public String nowProgram(){
        rnd = new Random();
        int i = rnd.nextInt(3);
        return programs[i].getNameProgram();
    }

    @Override
    public String toString() {
        return "Текущий канал: " + nameChannel + " ";
    }
}
