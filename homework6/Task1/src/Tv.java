public class Tv {
    private String model;
    private String name;

    private RemoteController remoteController;
    private Channel channels[];
    private int countOfChannel;

    public Tv(String model, String name) {
        this.model = model;
        this.name = name;
        this.channels = new Channel[3];
    }

    public String getModel() {
        return model;
    }

    public String getName() {
        return name;
    }

    public void takeRemoteController(RemoteController remoteController){
        this.remoteController = remoteController;
    }

    public void takeChannel(Channel channel){
        this.channels[countOfChannel] = channel;
        this.countOfChannel++;
    }

    public void show(int numberOfChannel){
        if(numberOfChannel >= 0 && numberOfChannel < countOfChannel) {
            System.out.println(channels[numberOfChannel].nowProgram());
        } else {
            System.out.println("Указан неверный канал");
        }

        System.out.println(channels[numberOfChannel]);
    }
}
